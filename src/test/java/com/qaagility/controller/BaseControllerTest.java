package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}
	
	@Test
	public void testDivideValid() throws Exception{
		final int val = new Count().divide(6,2);
		assertEquals("Divide",val,3);
	}

	@Test
        public void testDivideZero() throws Exception{
                final int val = new Count().divide(6,0);
                assertEquals("Divide",val,2147483647);
        }
	

	@Test
        public void testCalcMul() throws Exception{
                final int val = new Calcmul().mul();
                assertEquals("Multiply",val,18);
        }

	@Test
        public void testCalcAdd() throws Exception{
                final int val = new Calculator().add();
                assertEquals("Multiply",val,9);
        }


	@Test
        public void testAbout() throws Exception{
                final String val = new About().desc();
                assertTrue("Police",val.contains("police"));
        }



}
